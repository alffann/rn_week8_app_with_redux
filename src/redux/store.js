import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import LoginReducer from './reducers/LoginReducer';
import RegisterReducer from './reducers/RegisterReducer';
import HomeReducer from './reducers/HomeReducer';
import SearchReducer from './reducers/SearchReducer';
import FavoriteReducer from './reducers/FavoriteReducer';
import BookDetailReducer from './reducers/BookDetailReducer';

const allReducers = combineReducers({
    LoginReducer,
    RegisterReducer, 
    HomeReducer,
    FavoriteReducer,
    SearchReducer,
    BookDetailReducer,
})

export const bookStore = createStore(allReducers, applyMiddleware(thunk))
