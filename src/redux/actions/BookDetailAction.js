import axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage';

// Definisikan properti book detail
export const BOOK_DETAIL_START = 'BOOK_DETAIL_START';
export const BOOK_DETAIL_SUCCESS = 'BOOK_DETAIL_SUCCESS';
export const BOOK_DETAIL_FAILED = 'BOOK_DETAIL_FAILED';

export const DetailBook = (bookId) => async dispatch => {
    try{
        // Fetching data book detail
        dispatch({type: BOOK_DETAIL_START})
        const tokenDariLocalStorage = await AsyncStorage.getItem('token-user');
        const detailBookResponse = await axios.get(`http://code.aldipee.com/api/v1/books/${bookId}`, {
            headers: {
                Authorization: `Bearer ${tokenDariLocalStorage}`
            }}
        )

        // Jika status detail book response adalah 200, maka sukses fetching data
        if (detailBookResponse.status === 200) {
            dispatch({
                type: BOOK_DETAIL_SUCCESS,
                dataBookDetail: detailBookResponse.data,
            })
        }
    } catch(error) {
        dispatch({
            type: BOOK_DETAIL_FAILED,
            errorMessage: error.response.data.message
        })
        console.log(error.response.data.message, 'ini message error')
    }
}
