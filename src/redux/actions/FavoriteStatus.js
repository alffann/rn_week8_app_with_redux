import axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage';

// Definisikan properti favorite status
export const FAVORITE_STATUS_START = 'FAVORITE_STATUS_START';
export const FAVORITE_STATUS_SUCCESS = 'FAVORITE_STATUS_SUCCESS';
export const FAVORITE_STATUS_FAILED = 'FAVORITE_STATUS_FAILED';

export const FavoriteStatus = (bookId) => async dispatch => {
    try{
        // Fetching data all favorite
        dispatch({type: FAVORITE_STATUS_START})
        const tokenDariLocalStorage = await AsyncStorage.getItem('token-user');
        const fetchingAllFavorite = await axios.get('http://code.aldipee.com/api/v1/books/my-favorite', {
            headers: {
                Authorization: `Bearer ${tokenDariLocalStorage}`
            }}
        )
        const totalResults = fetchingAllFavorite.data.totalResults

        // Fetching data favorite status
        const FavoriteStatusResponse = await axios.get(`http://code.aldipee.com/api/v1/books/my-favorite?limit=${totalResults}`, {
            headers: {
                Authorization: `Bearer ${tokenDariLocalStorage}`
            }}
        )

        // Jika status favorite status response adalah 200, maka sukses fetching data
        if (FavoriteStatusResponse.status === 200) {
            const isFavorite = FavoriteStatusResponse.data.some(x => x._id == bookId)
            dispatch({
                type: FAVORITE_STATUS_SUCCESS,
                dataIsFavorite: isFavorite, 
            })
            console.log(isFavorite, 'ini status favorite')
        }
    } catch(error) {
        dispatch({
            type: FAVORITE_STATUS_FAILED,
            errorMessage: error.response.data.message
        })
        console.log(error.response.data.message, 'ini message error')
    }
}
