import axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage';

// Definisikan properti book list
export const BOOK_LIST_START = 'BOOK_LIST_START';
export const BOOK_LIST_SUCCESS = 'BOOK_LIST_SUCCESS';
export const BOOK_LIST_FAILED = 'BOOK_LIST_FAILED';

export const BookList = () => async dispatch => {
    try{
        // Fetching data semua buku
        dispatch({type: BOOK_LIST_START})
        const tokenDariLocalStorage = await AsyncStorage.getItem('token-user');
        const fetchingAllBook = await axios.get('http://code.aldipee.com/api/v1/books', {
            headers: {
                Authorization: `Bearer ${tokenDariLocalStorage}`
            }}
        )
        const totalResults = fetchingAllBook.data.totalResults

        // Fetching data book list
        const bookListResponse = await axios.get(`http://code.aldipee.com/api/v1/books?limit=${totalResults}`, {
            headers: {
                Authorization: `Bearer ${tokenDariLocalStorage}`
            }}
        )

        // Jika status book list response adalah 200, maka sukses fetching data
        if (bookListResponse.status === 200) {
            dispatch({
                type: BOOK_LIST_SUCCESS,
                dataBookList: bookListResponse.data.results,
            })
        }
    } catch(error) {
        dispatch({
            type: BOOK_LIST_FAILED,
            errorMessage: error.response.data.message
        })
        console.log(error.response.data.message, 'ini message error')
    }
}