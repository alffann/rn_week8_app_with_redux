import axios from 'axios' // Inport Axios

// Definisikan properti Register
export const REGISTER_START = 'REGISTER_START';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILED = 'REGISTER_FAILED';
export const STATUS_REGISTER = 'STATUS_REGISTER';

export const RegisterAction = reqRegister => async dispatch => {
    try{
        // Fetching data register
        dispatch({type: REGISTER_START})
        const url = 'http://code.aldipee.com/api/v1/auth/register'
        const RegisterResponse = await axios.post(url, reqRegister)
        // Jika status register response adalah 201, maka sukses register
        if (RegisterResponse.status === 201){
            dispatch({type: REGISTER_SUCCESS})
        }

    } catch(error){
        dispatch({
            type: REGISTER_FAILED,
            errorMessage: error.RegisterResponse.data.message
        })
    }
}

export const updateStatusReg = () => {
    return {type: STATUS_REGISTER}
}