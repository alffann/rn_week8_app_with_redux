import axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage';

// Definisikan properti book favorite
export const BOOK_FAVORITE_START = 'BOOK_FAVORITE_START';
export const BOOK_FAVORITE_SUCCESS = 'BOOK_FAVORITE_SUCCESS';
export const BOOK_FAVORITE_FAILED = 'BOOK_FAVORITE_FAILED';

export const BookFavorite = () => async dispatch => {
    try{
        // Fetching semua data book favorite
        dispatch({type: BOOK_FAVORITE_START})
        const tokenDariLocalStorage = await AsyncStorage.getItem('token-user');
        const fetchingAllBookFav = await axios.get('http://code.aldipee.com/api/v1/books/my-favorite', {
            headers: {
                Authorization: `Bearer ${tokenDariLocalStorage}`
            }}
        )
        const totalResults = fetchingAllBookFav.data.totalResults

        // Fetching data book favorite
        const bookFavResponse = await axios.get(`http://code.aldipee.com/api/v1/books/my-favorite?limit=${totalResults}`, {
            headers: {
                Authorization: `Bearer ${tokenDariLocalStorage}`
            }}
        )

        // Jika status book favorite response adalah 200, maka sukses fetching data
        if (bookFavResponse.status === 200) {
            dispatch({
                type: BOOK_FAVORITE_SUCCESS,
                dataBookFavorite: bookFavResponse.data,
            })
        }
    } catch(error) {
        dispatch({
            type: BOOK_FAVORITE_FAILED,
            errorMessage: error.response.data.message
        })
    }
}