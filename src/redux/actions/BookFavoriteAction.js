import axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage';

// Definisikan properti favorite book
export const FAVORITE_BOOK_START = 'FAVORITE_BOOK_START';
export const FAVORITE_BOOK_SUCCESS = 'FAVORITE_BOOK_SUCCESS';
export const FAVORITE_BOOK_FAILED = 'FAVORITE_BOOK_FAILED';

export const FavoriteBook = requestBodyFav => async dispatch => {
    try{
        // Fetching data favorite book
        dispatch({type: FAVORITE_BOOK_START})
        const tokenDariLocalStorage = await AsyncStorage.getItem('token-user');
        const favoriteBookResponse = await axios.post(
            'http://code.aldipee.com/api/v1/books/my-favorite', 
            requestBodyFav,
            { headers: {
                Authorization: `Bearer ${tokenDariLocalStorage}`
            }},
        )

        // Jika status favorite book response adalah 201, maka sukses fetching data
        if (favoriteBookResponse.status === 201){
            dispatch({type: FAVORITE_BOOK_SUCCESS})
        }
    } catch(error){
        dispatch({
            type: FAVORITE_BOOK_FAILED,
            errorMessage: error.favoriteBookResponse.data.message
        })
    }
}


