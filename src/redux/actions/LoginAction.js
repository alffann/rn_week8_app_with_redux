import axios from 'axios'; // import Axios
import AsyncStorage from '@react-native-async-storage/async-storage'; // import RN Async Storage

// Definisikan properti Login
export const LOGIN_START = 'LOGIN_START';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const STATUS_LOGIN = 'STATUS_LOGIN';

export const LoginAction = reqLogin => async dispatch => {
    try{
        // Fetching data login
        dispatch({type: LOGIN_START})
        const url = 'http://code.aldipee.com/api/v1/auth/login'
        const LoginResponse = await axios.post(url, reqLogin)
        // Jika status login response adalah 200, maka sukses login
        if (LoginResponse.status === 200) {
            dispatch({
                type: LOGIN_SUCCESS,
                name: LoginResponse.data.user.name,
            })
            // Menyimpan token user login
            await AsyncStorage.setItem('token-user',
                LoginResponse.data.tokens.access.token,
            );
        }

    } catch(error){
        dispatch({
            type: LOGIN_FAILED,
            errorMessage: error.LoginResponse.data.message
        })
    }
}

export const updateStatusLogin = () => {
    return {type: STATUS_LOGIN}
}
