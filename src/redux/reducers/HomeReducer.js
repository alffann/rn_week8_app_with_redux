import {
    BOOK_LIST_START,
    BOOK_LIST_SUCCESS,
    BOOK_LIST_FAILED,
} from './../actions/HomeAction'

// Mendefinisikan awal state
const StateAwal = {
    listBook: [],
    isLoading: false,
    errorMessage: null,
} 

export default (state = StateAwal, action) => {
    switch (action.type) {
        case BOOK_LIST_SUCCESS:
            return { ...state, listBook: action.dataBookList}
        case BOOK_LIST_FAILED:
            return { ...state, errorMessage: action.errorMessage}
    default:
        return state
    }
}
