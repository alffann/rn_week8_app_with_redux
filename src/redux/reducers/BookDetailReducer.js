import {
    BOOK_DETAIL_START,
    BOOK_DETAIL_SUCCESS,
    BOOK_DETAIL_FAILED,
} from './../actions/BookDetailAction';

import {
    FAVORITE_BOOK_START,
    FAVORITE_BOOK_SUCCESS,
    FAVORITE_BOOK_FAILED,
} from './../actions/BookFavoriteAction';

import {
    FAVORITE_STATUS_START,
    FAVORITE_STATUS_SUCCESS,
    FAVORITE_STATUS_FAILED,
} from './../actions/FavoriteStatus';

// Mendefinisikan awal state
const StateAwal = {
    bookDetail: {},
    isLoading: false,
    isFavorite: false,
    favoriteBook: false,
    errorMessage: null,
} 

export default (state = StateAwal, action) => {
    switch (action.type) {
        case BOOK_DETAIL_START:
            return { ...state, isLoading: true }        
        case BOOK_DETAIL_SUCCESS:
            return { ...state, bookDetail: action.dataBookDetail, isLoading: false }
        case BOOK_DETAIL_FAILED:
            return { ...state, errorMessage: action.errorMessage, isLoading: false}

        case FAVORITE_BOOK_SUCCESS:
            return { ...state, postFavorite: true}

        case FAVORITE_STATUS_SUCCESS:
            return { ...state, isFavorite: action.dataIsFavorite}
    
    default:
        return state
    }
}
