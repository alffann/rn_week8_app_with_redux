import {
    REGISTER_START,
    REGISTER_SUCCESS,
    REGISTER_FAILED,
    STATUS_REGISTER
} from './../actions/RegisterAction'

// Mendefinisikan awal state
const StateAwal = {
    isRegisterSuccess: false,
    isLoading: false,
    errorMessage: null,
}

export default (state = StateAwal, action) => {
    switch (action.type) {
        case REGISTER_START:
            return { state }
        case REGISTER_SUCCESS:
            return { ...state, isRegisterSuccess: true, errorMessage: null}
        case REGISTER_FAILED:
            return { ...state, errorMessage: action.errorMessage}
        case STATUS_REGISTER:
            return { ...state, isRegisterSuccess: false}

        default:
            return state
    }
}
