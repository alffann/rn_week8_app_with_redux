import {
    BOOK_FAVORITE_START,
    BOOK_FAVORITE_SUCCESS,
    BOOK_FAVORITE_FAILED,
} from './../actions/FavoriteAction'

// Mendefinisikan awal state
const StateAwal = {
    listBookFavorite: [],
    isLoading: false,
    errorMessage: null,
}
 
export default (state = StateAwal, action) => {
    switch (action.type) {
        case BOOK_FAVORITE_START:
            return { ...state, isLoading: true }
        case BOOK_FAVORITE_SUCCESS:
            return { ...state, listBookFavorite: action.dataBookFavorite, isLoading: false }
        case BOOK_FAVORITE_FAILED:
            return { ...state, errorMessage: action.errorMessage}

    default:
        return state
    }
}
