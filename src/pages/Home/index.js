import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableOpacity, Image, StyleSheet } from 'react-native'
import {connect} from 'react-redux'
import {updateStatusLogin} from '../../redux/actions/LoginAction'
import {BookList} from '../../redux/actions/HomeAction'
import {styles} from './style'

export class Home extends Component {
    async componentDidMount(){
        await this.props.updateStatusLogin()
        this.props.BookList()
    }

    render() {
        return (
            <ScrollView>
            <View>
                {/* Menampilkan welcoming text */}
                <Text style={styles.halloText}> Hallo {this.props.dataLogin.name}</Text>

                {/* Menampilkan sub menu recommended */}
                <Text style={styles.sectionText}> Recommended </Text>
                <ScrollView horizontal={true}>
                    {/* Menampilkan list book berupa poster */}
                    <View style={styles.rekomenBookListContainer}>
                        {this.props.dataBook.listBook.map(book => (
                            <TouchableOpacity key={book.id}
                                onPress={()=>{
                                    this.props.navigation.navigate('BookDetail', {bookId: book.id})
                                }}                                        
                            >
                                <View style={styles.rekomenBookContainer}>
                                    {/* Menampilkan poster buku */}
                                    <Image style={styles.rekomenBookImage}
                                        source={{uri: book.cover_image}}
                                    />
                                    {/* Menampilkan judul buku */}
                                    <Text style={styles.rekomenBookTitle}>{book.title}</Text>
                                </View>
                            </TouchableOpacity>
                        ))}
                    </View>
                </ScrollView>

                {/* Menampilkan sub menu popular book */}
                <Text style={styles.sectionText}> Popular Book </Text>
                {/* Menampilkan list book berupa poster */}
                <View style={styles.populerBookListContainer}>
                    {this.props.dataBook.listBook.map(book => (
                        <TouchableOpacity key={book.id}
                            onPress={()=>{
                                this.props.navigation.navigate('BookDetail', {BookId: book.id})
                            }}                                        
                            style={styles.populerBookContainer}
                        >
                            {/* Menampilkan poster buku */}
                            <Image style={styles.populerBookImage}
                                source={{uri: book.cover_image}}
                            />
                            {/* Menampilkan judul buku */}
                            <Text style={styles.populerBookTitle}>{book.title}</Text>

                        </TouchableOpacity>
                    ))}
                </View>
            </View>
            </ScrollView>
        )
    }
}

const mapStateToProps = (state) => ({
    dataLogin: state.LoginReducer,
    dataBook: state.HomeReducer
})
  
const mapDispatchToProps = {
    updateStatusLogin,
    BookList
}
  
export default connect(mapStateToProps, mapDispatchToProps) (Home)
