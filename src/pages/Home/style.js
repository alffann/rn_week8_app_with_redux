import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
        halloText: {
            fontSize: 20,
            padding: 10,
            fontStyle: 'italic'
        },
        sectionText: {
            marginVertical: 10,
            marginLeft: 10,
            fontSize: 20,
            fontWeight: 'bold'
        },
        textJudulFilm: {
            fontSize: 22,
            marginBottom: 10,
            marginRight: 5,
            fontFamily: 'JosefinSans-Bold',
        },
        scrollView: {
            padding: 10,
            backgroundColor: '#FF5E59'
          },
        contentContainer: {
            backgroundColor: 'lightgrey',
            paddingBottom: 50
        },
        rekomenBookListContainer: {
            flexDirection: 'row',
        },
        rekomenBookContainer: {
            alignItems: 'center',
            marginHorizontal: 5,
            width: 500*0.3,
        },
        rekomenBookImage: {
            width: 500*0.3,
            height: 750*0.3,
            borderRadius: 15,
            marginHorizontal: 5,
        },
        rekomenBookTitle: {
            textAlign: 'center',
            fontSize: 10,
        },
        populerBookListContainer: {
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            marginHorizontal: 10,
            flexWrap: 'wrap',
        },
        populerBookContainer: {
            alignItems: 'center',
            marginBottom: 10,
            marginHorizontal: 5,
            width: 500*0.2,
        },
        populerBookImage: {
            width: 500*0.22,
            height: 750*0.22,
            borderRadius: 15,
        },
        populerBookTitle: {
            textAlign: 'center',
            fontSize: 10,
            fontWeight: 'bold',
        },
    });
    