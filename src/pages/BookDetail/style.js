import { StyleSheet, Dimensions } from "react-native";

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({

    loading: {
        width: windowWidth,
        height: windowHeight,
        justifyContent: 'center',
        alignItems: 'center',
    },

    // TopIcon
    IconContainer: {
        flexDirection: 'row',
        margin: 10,
    },
    topIconBack: { 
        padding: 3, 
        marginLeft: 10,
        marginRight: 250,
        backgroundColor: 'green', 
        borderRadius: 20, 
        alignItems: 'center'
    },
    topIconFav: { 
        padding: 3, 
        marginHorizontal: 10,
        backgroundColor: 'green', 
        borderRadius: 20,
        opacity: 0.75,
        alignItems: 'center'
    },
    topIconShare: { 
        padding: 3, 
        marginHorizontal: 10,
        backgroundColor: 'green', 
        borderRadius: 20, 
        opacity: 0.75,
        alignItems: 'center'
    },

    // Top container
    topContainer: {
        backgroundColor: 'white',
        padding: 17,
        marginHorizontal: 30,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
        marginTop: 10,
        flexDirection: 'row',
    },
    smallPoster: {
        width: 500*0.2,
        height: 750*0.2,
        marginRight: 15,
        borderRadius: 10,
    },
    titleText: {
        fontSize: 15,
    },
    authorText: {
        fontSize: 14,
    },
    ratingView: {
        flex: 1,
    },
    rightTopContainer: {
        flex: 1,
        flexWrap: 'wrap',
        alignItems: 'flex-start'
    },

    // Mid container
    midContainer: {
        backgroundColor: 'white',
        padding: 17,
        marginHorizontal: 30,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    midContainerItem: {
        alignItems: 'center'
    },
    midContainerButton: {
        flexDirection: 'row',
        backgroundColor: 'green',
        borderRadius: 10,
        paddingHorizontal: 10,
        alignItems:'center'
    },
    midContainerText: {
        fontSize: 15,
    },
    midContainerNumber: {
        fontSize: 15,
    },
    midContainerButtonText: {
        color: 'white',
    },
    
    // Bottom Container
    bottomContainer: {
        padding: 17,
        marginHorizontal: 30,
        borderRadius: 10,
        marginTop: 10,
    },
    overviewText: {
        fontSize: 25,
        marginBottom: 10,      
    },
    contentText: {
        fontSize: 15,
    },

})

