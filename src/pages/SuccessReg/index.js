import React, { Component } from 'react'
import {
    Text,
    View,
    TouchableOpacity,
    Image
} from 'react-native'
import {connect} from 'react-redux'
import {updateStatusReg} from '../../redux/actions/RegisterAction'
import {SuccessLogo} from '../../assets';
import {styles} from './style'

export class SuccessReg extends Component {
    constructor(){
        super()
    }

    async componentDidMount(){
        await this.props.updateStatusReg()
    }

    render() {
        return (
            <View style={styles.container}>
                {/* Menampilkan header */}
                <Text style={styles.headerText}>Registration Completed!</Text>
                {/* Menampilkan logo sukses*/}
                <Image source={SuccessLogo} style={styles.successLogo}/>
                {/* Menampilkan pesan sukses */}
                <Text style={styles.pesanText}>We sent email verification to your email</Text>
                {/* Menampilkan tombol back to login */}
                <TouchableOpacity style={styles.buttonKembali} onPress={()=>{this.props.navigation.navigate('Login')}}>
                    <Text style={styles.buttonText}>Back to Login</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    dataRegister: state.RegisterReducer
})
  
const mapDispatchToProps = {
    updateStatusReg
}
  
export default connect(mapStateToProps, mapDispatchToProps) (SuccessReg)
