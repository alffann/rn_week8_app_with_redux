import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 40,
    },
    successLogo: {
        width: 230,
        height: 205,
        marginVertical: 60,
    },
    headerText: {
        fontSize: 25,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    pesanText: {
        fontSize: 15,
        textAlign: 'center',
    },
    buttonText: {
        fontSize: 15,
        color: 'white'
    },
    buttonKembali: {
        marginVertical: 30,
        backgroundColor: 'green',
        paddingHorizontal: 30,
        paddingVertical: 10,
        borderRadius: 10,
    }
})