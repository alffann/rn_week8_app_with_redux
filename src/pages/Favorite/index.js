import React, { Component } from 'react'
import { 
    Text, 
    View, 
    TouchableOpacity, 
    Image, 
    ScrollView, 
    RefreshControl,
    ActivityIndicator,
    ImageBackground
} from 'react-native'
import {connect} from 'react-redux'
import {BookFavorite} from '../../redux/actions/FavoriteAction'
import { SplashBackground } from '../../assets';
import {styles} from './style'

export class Favorite extends Component {
    constructor(){
        super()
        this.state = {
            refreshing: false,
        }
    }

    componentDidMount(){
        this.props.BookFavorite()
    }

    // Menampilkan fitur on-refresh
    onRefresh = async () => {
        this.setState({refreshing: true});
        this.props.BookFavorite()
        this.setState({refreshing: false});
    } 

    render() {
        return (
            <>            
            <ScrollView 
                style={{backgroundColor: 'transparent'}}
                refreshControl={
                    <RefreshControl 
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh}
                    />
                }
            >

            {this.props.dataBookFavorite.isLoading ? (
                <View style={styles.loading}>
                    <ActivityIndicator size='large' color='red'/>
                </View>
            ): 
                <View>
                {/* Menampilkan header favorite book */}    
                <Text style={styles.sectionText}> Favorite Books </Text>

                <View>
                    {this.props.dataBookFavorite.listBookFavorite.map((book, index) => (
                        <>
                        <View style={styles.favBookContainer(index)}>
                            {/* Menampilkan fitur navigasi ke book detail */} 
                            <TouchableOpacity key={book._id}
                                onPress={()=>{
                                    this.props.navigation.navigate('BookDetail', {BookId: book._id})
                                }}                                        
                            >
                                {/* Menampilkan poster buku favorit */} 
                                <Image style={styles.favBookImage}
                                    source={{
                                        uri: book.cover_image,
                                    }}
                                />
                            </TouchableOpacity>
                            {/* Menampilkan judul, penulis, dan pencetak buku */} 
                            <View style={styles.rightFavContainer}>
                                <Text style={styles.titleText}>{book.title}</Text>
                                <Text style={styles.authorText}>{book.author}</Text>
                                <Text style={styles.authorText}>{book.publisher}</Text>
                            </View>
                        </View>
                        </>
                    ))}
                </View>
                </View>
            }
            </ScrollView>
            </>
        )
    }
}

const mapStateToProps = (state) => ({
    dataBookFavorite: state.FavoriteReducer
})
  
const mapDispatchToProps = {
    BookFavorite
}
  
export default connect(mapStateToProps, mapDispatchToProps) (Favorite)
