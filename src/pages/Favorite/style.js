import { StyleSheet, Dimensions } from "react-native";

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
 
    background: {
        position:'absolute',
        left:0, 
        right:0, 
        top:0,
        bottom:0
    },
    loading: {
        width: windowWidth,
        height: windowHeight,
        justifyContent: 'center',
        alignItems: 'center',
    },
    sectionText: {
        marginVertical: 10,
        marginLeft: 10,
        fontSize: 22,
        textAlign: 'center',
    },
    favBookContainer: (index) => ({
        backgroundColor: '#FFEFD5',
        paddingVertical: 5,
        paddingHorizontal: 20,
        marginHorizontal: 30,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
        marginTop: 10,
        flexDirection: 'row',
    }),
    rightFavContainer: {
        flex: 1,
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        justifyContent: 'center',
        marginLeft: 10
    },

    favBookImage: {
        width: 500*0.15,
        height: 750*0.15,
        borderRadius: 15,
    },
    titleText: {
        fontSize: 15,
    },
    authorText: {
        fontSize: 13,
    },
});
