import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        flex: 1, 
    },
    background: {
        flex: 1, // pushes the footer to the end of the screen
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        width: 230,
        height: 205,
    },
    name: {
        color: 'darkgrey',
        height: 30,       
        textAlign: 'center',
    }
})
