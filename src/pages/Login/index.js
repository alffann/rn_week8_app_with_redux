import React, { Component } from 'react'
import {
    Text,
    View,
    TextInput,
    TouchableOpacity
} from 'react-native'
import {connect} from 'react-redux'
import {LoginAction} from '../../redux/actions/LoginAction'
import {styles} from './style'

export class Login extends Component {
    constructor(){
        super();
        // Mendefinisikan state awal
        this.state = {
          email: '',
          password: '',
          isLoading: false
        }
    }

    // Jika login sukses, lanjut ke halaman MainApp (Home, Seacrh, Favorite)
    componentDidUpdate(){
        if (this.props.dataLogin.isLoginSuccess === true){
            this.props.navigation.navigate('MainApp')
        }
    }

    // Menghandle perubahan nilai email
    handleChangeEmail = (email) => {
        this.setState({email})
    }

    // Menghandle perubahan nilai password    
    handleChangePassword = (password) => {
    this.setState({password})
    }

    // Menghandle submit item
    handleSubmitItem = () => {
        const submitData = {
            email: this.state.email,
            password: this.state.password,
        }
        this.props.LoginAction(submitData)
    }

    render() {
        return (
            <View style={styles.container}>

                {/* Menampilkan pesan error */}
                {this.props.dataLogin.errorMessage !== null ? 
                    <View style={styles.errorView}>
                        <Text style={styles.errorText}>
                            {this.props.dataLogin.errorMessage}
                        </Text> 
                    </View> : null
                }

                <View>
                    {/* Input email */}
                    <TextInput 
                        onChangeText={this.handleChangeEmail} 
                        style={styles.inputData} 
                        placeholder='Masukkan Email'
                        placeholderTextColor='grey'
                    /> 
                    {/* Input password */}
                    <TextInput 
                        onChangeText={this.handleChangePassword} 
                        style={styles.inputData} 
                        placeholder='Masukkan Password'
                        placeholderTextColor='grey'
                        secureTextEntry={true}
                    /> 
                </View>    

                {/* Menampilkan tombol login */}
                <TouchableOpacity style={styles.loginButton} onPress={this.handleSubmitItem}>
                    <Text style={styles.loginButtonText}>Login</Text>
                </TouchableOpacity>

                {/* Menampilkan pilihan register */}
                <Text>Don't have an account?</Text>
                <TouchableOpacity style={styles.registerButton}
                    onPress={()=>{
                        this.props.navigation.navigate('Register')
                    }}                                                        >
                    <Text style={styles.registerButtonText}>Register</Text>
                </TouchableOpacity>

            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    dataLogin: state.LoginReducer
})
  
const mapDispatchToProps = {
    LoginAction
}
  
export default connect(mapStateToProps, mapDispatchToProps) (Login)
